const path = require('path');
const production = process.env.NODE_ENV === 'production';
const TerserPlugin = require('terser-webpack-plugin');

module.exports = {
	mode: production ? 'production' : 'development',
	devtool: production ? false : 'inline-source-map',
	entry: './src/webpack-main.ts',
	output: {
		path: path.join(__dirname, 'dist'),
		filename: 'novum-erd.js',
		libraryTarget: 'var',
		library: 'Novum'
	},
	resolve: {
		extensions: ['.ts', '.tsx', '.js', '.jsx'],
		modules : [
			'node_modules'
		]
	},
	optimization: {
		minimizer: [
			new TerserPlugin({
				parallel: true,
				terserOptions: {
					ecma: 8
				}
			})
		]
	},
	module: {
		rules: [
			{
				test: /\.css$/,
				use: ['style-loader', 'css-loader']
			},
			{
				test: /\.tsx?$/,
				loader: 'ts-loader',
				options: {
					transpileOnly: true
				}
			},
			{
				test: /\.ts?$/,
				loader: 'ts-loader',
				options: {
					transpileOnly: true
				}
			}
		]
	},
	node: {
		fs: "empty"
	},
	devServer: {
		host: '0.0.0.0',
		compress: true,
		port: 9000,
		disableHostCheck: true,
		overlay: true
	}
};
