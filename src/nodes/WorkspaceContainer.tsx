import * as React from 'react';
import {TopButton, TopButtonRight, WorkspaceWidget} from '../system/WorkspaceWidget';
import {CanvasWidget} from '@projectstorm/react-canvas-core/';
import {CanvasContainerWidget} from '../system/CanvasContainerWidget';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faCogs, faSearchMinus, faSearchPlus, faTable, faExpandArrowsAlt} from '@fortawesome/free-solid-svg-icons'
import {Messenger} from "../system/Messenger";
import {Renderer} from "../system/Renderer";
import {DistributeHelper} from "../system/DistributeHelper";
import {ILogger} from "../util/ILogger";

export class WorkspaceContainer extends React.Component<{ renderer: Renderer, logger : ILogger }, any> {

    constructor(props) {
        super(props);
        this.zoom = this.zoom.bind(this);
        this.zoomToFit = this.zoomToFit.bind(this);

        this.zoomOut = this.zoomOut.bind(this);
        this.zoomIn = this.zoomIn.bind(this);

        this.stopZoom = this.stopZoom.bind(this);


        this.autoDistribute = this.autoDistribute.bind(this);

    }

    private zoomInterval;

    componentDidMount(): void {
        /*
        setTimeout(() => {
            this.autoDistribute();
            this.props.renderer.getEngine().zoomToFit();
        }, 10);

         */
    }


    private zoom(zoomDifference) {

        this.props.logger.info('factor: ' + zoomDifference + ', level: ' + this.props.renderer.getEngine().getModel().getZoomLevel());

        let zoomLevel = this.props.renderer.getEngine().getModel().getZoomLevel();
        console.log('zoom level ' + zoomLevel);
        this.props.renderer.getEngine().getModel().setZoomLevel(zoomLevel + zoomDifference);
        this.props.renderer.getEngine().repaintCanvas();

    }

    stopZoom() {
        clearInterval(this.zoomInterval);
    }

    zoomOut() {
        console.log('zoom out');
        this.props.logger.info('WorkspaceContainer.startZoomIn()');
        this.zoomInterval = setInterval(() => {
            this.zoom(-2.1);
        }, 50);

    }

    zoomIn() {
        console.log('zoom in');
        this.props.logger.info('WorkspaceContainer.startZoomIn()');
        this.zoomInterval = setInterval(() => {
            this.zoom(2.1);
        }, 50);
    }

    zoomToFit(): void {
        this.props.logger.info("WorkspaceContainer.zoomToFit");
        DistributeHelper.distribute(this.props.renderer.getEngine(), this.props.logger);
        this.props.renderer.getEngine().zoomToFit();
    };

    autoDistribute(): void {
        this.props.logger.info("WorkspaceContainer.autoDistribute");
        DistributeHelper.distribute(this.props.renderer.getEngine(), this.props.logger);
        this.props.renderer.getEngine().repaintCanvas();
    };

    render() {
        this.props.logger.info('render');
        const zoomToFitIcon = (<FontAwesomeIcon icon={faExpandArrowsAlt}/>);
        const zoomInIcon = (<FontAwesomeIcon icon={faSearchPlus}/>);
        const zoomOutIcon = (<FontAwesomeIcon icon={faSearchMinus}/>);
        const modelIcon = (<FontAwesomeIcon icon={faTable}/>);
        const cogsIcon = (<FontAwesomeIcon icon={faCogs}/>);


        let buttons = (
            <React.Fragment>
                <TopButton onClick={this.autoDistribute}> Re-distribute</TopButton>
                <TopButton onClick={this.zoomToFit}>{zoomToFitIcon} Zoom to fit</TopButton>
                <TopButton onMouseDown={this.zoomIn} onMouseUp={this.stopZoom}>{zoomInIcon} Zoom in</TopButton>
                <TopButton onMouseDown={this.zoomOut} onMouseUp={this.stopZoom}>{zoomOutIcon} Zoom out</TopButton>
                <TopButton onClick={() => Messenger.send('add_model', {})}>{modelIcon} Add model</TopButton>
                <TopButtonRight onClick={() => Messenger.send('deploy', {deploy: true})}>{cogsIcon} Deploy</TopButtonRight>

            </React.Fragment>
        );

        return (
            <WorkspaceWidget buttons={buttons}> <CanvasContainerWidget>
                <CanvasWidget engine={this.props.renderer.getEngine()}/> </CanvasContainerWidget> </WorkspaceWidget>
        );
    }
}
