import * as React from 'react';
import {ErdNodeModel, ErdNodeModelGenerics} from './ErdNodeModel';
import { ErdNodeWidget } from './ErdNodeWidget';
import {AbstractReactFactory} from '@projectstorm/react-canvas-core/';
import {GenerateModelEvent} from '@projectstorm/react-canvas-core/';
import { DiagramEngine } from '@projectstorm/react-diagrams-core/';
import {BaseModel} from "@projectstorm/react-canvas-core/";
import {BaseModelGenerics} from "@projectstorm/react-canvas-core/";
import {CanvasEngine} from "@projectstorm/react-canvas-core/";
import {ErdLinkModel, ErdLinkModelGenerics} from "./ErdLinkModel";
import {LinkWidget} from "@projectstorm/react-diagrams-core/";

export class ErdLinkFactory extends AbstractReactFactory<ErdLinkModel, DiagramEngine> {

	constructor() {
		super('ts-custom-link');
	}

	generateModel(event : GenerateModelEvent):ErdLinkModel<ErdLinkModelGenerics> {
		return new ErdLinkModel(new ErdLinkModelGenerics());
	}

	generateReactWidget(event): JSX.Element {
		return <LinkWidget link={new ErdLinkModel(new ErdLinkModelGenerics())} diagramEngine={this.engine} />;
	}

}
