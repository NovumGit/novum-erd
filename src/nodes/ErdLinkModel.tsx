import {
    LinkModel,
    LinkModelGenerics,
    LinkModelListener
} from "@projectstorm/react-diagrams-core/";
import {BaseModelOptions} from "@projectstorm/react-canvas-core/";
import {DiagramModel} from "@projectstorm/react-diagrams-core/";

export class ErdLinkModelGenerics implements LinkModelGenerics, BaseModelOptions{
    LISTENER: LinkModelListener;
    PARENT: DiagramModel;
    OPTIONS : BaseModelOptions

    type?: string;
    selected?: boolean;
    extras?: any;
}


export class ErdLinkModel<G extends LinkModelGenerics = ErdLinkModelGenerics> extends LinkModel<G> {

}
