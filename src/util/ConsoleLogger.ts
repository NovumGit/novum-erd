import {ILogger} from "./ILogger";
import {LogLevel} from "../types/LogLevel";

export class ConsoleLogger implements ILogger {

    private logLevel : string;

    constructor(logLevel : string, loggerImplementation? : ILogger) {
        this.logLevel = logLevel;
    }

    emerg(message: string): void {
        if(this.logLevel == LogLevel.none)
        {
            return;
        }
    }

    alert(message: string): void {
        if(this.logLevel == LogLevel.none)
        {
            return;
        }
    }

    crit(message: string): void {
        if(this.logLevel == LogLevel.none)
        {
            return;
        }
    }

    error(message: string): void {
        if(this.logLevel == LogLevel.none)
        {
            return;
        }
    }

    warning(message: string): void {
        if(this.logLevel == LogLevel.none)
        {
            return;
        }
    }

    notice(message: string): void {
        if(this.logLevel == LogLevel.none)
        {
            return;
        }
    }

    info(message: string): void {
        if(this.logLevel == LogLevel.none)
        {
            return;
        }
    }

    debug(message: string): void {
        if(this.logLevel == LogLevel.none)
        {
            return;
        }
    }

}
