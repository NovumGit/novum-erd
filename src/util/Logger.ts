import winston from "winston";
import {ILogger} from "./ILogger";
import {LogLevel} from "../types/LogLevel";

export class Logger implements ILogger {

    private logger : ILogger;
    private logLevel : LogLevel;

    constructor(logLevel : string, loggerImplementation? : ILogger) {
        this.logLevel = logLevel;
        if(loggerImplementation)
        {
            this.logger = loggerImplementation;
        }
        else
        {
            this.logger = winston.createLogger({
                levels: winston.config.syslog.levels,
                transports: [
                    new winston.transports.Console({ level: logLevel })
                ]
            });
        }
    }

    emerg(message: string): void {
        if(this.logLevel == LogLevel.none)
        {
            return;
        }
        console.log(message);
        this.logger.emerg(message);
    }

    alert(message: string): void {
        if(this.logLevel == LogLevel.none)
        {
            return;
        }
        console.log(message);
        this.logger.alert(message);
    }

    crit(message: string): void {
        if(this.logLevel == LogLevel.none)
        {
            return;
        }
        console.log(message);
        this.logger.crit(message);
    }

    error(message: string): void {
        if(this.logLevel == LogLevel.none)
        {
            return;
        }
        console.log(message);
        this.logger.error(message);
    }

    warning(message: string): void {
        if(this.logLevel == LogLevel.none)
        {
            return;
        }
        console.log(message);
        this.logger.warning(message);
    }

    notice(message: string): void {
        if(this.logLevel == LogLevel.none)
        {
            return;
        }
        console.log(message);
        this.logger.notice(message);
    }

    info(message: string): void {
        if(this.logLevel == LogLevel.none)
        {
            return;
        }
        console.log(message);
        this.logger.info(message);
    }

    debug(message: string): void {
        if(this.logLevel == LogLevel.none)
        {
            return;
        }
        console.log(message);
        this.logger.debug(message);
    }

}
