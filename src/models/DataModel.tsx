import {Table} from "./Table";
import {Relation} from "./Relation";

export class DataModel
{
    tables? : Table[];
    relations? : Relation[];
}
