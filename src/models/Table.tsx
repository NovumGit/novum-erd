import {Field} from './Field';
import {MessageContentGenerics} from "../system/Messenger";

export interface ITable {
    id : number;
    module_id : number;
    api_expossed : boolean;
    api_description : string;
    name : string;
    title : string;
    fields : Field[];
}
export class Table implements MessageContentGenerics{

    id : number;
    module_id : number;
    api_expossed : boolean;
    api_description : string;
    name : string;
    title : string;
    fields : Field[];


    removeField(fieldToRemove : Field):void
    {
        let newFields : Field[] = [];
        this.fields.forEach((someField : Field) => {
            if(someField.name != fieldToRemove.name)
            {
                newFields.push(someField);
            }
            else
            {
                console.log('REMOVED', someField);
            }

        });
        this.fields = newFields;

    }
    static fromJson(tableJson : ITable):Table
    {
        const table = new Table();
        table.name = tableJson.name;
        table.title = tableJson.title;
        table.module_id = tableJson.module_id;
        table.api_description = tableJson.api_description;
        table.api_expossed = tableJson.api_expossed;
        return table;
    }

}
