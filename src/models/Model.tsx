import {DataModel} from "./DataModel";
import {Field} from "./Field";
import {Link} from "./Link";
import {Port} from "./Port";
import {Relation} from "./Relation";
import {Table} from "./Table";

export class Model {
    DataModel : DataModel;
    Field : Field;
    Link : Link;
    Port : Port;
    Relation : Relation;
    Table : Table;
}
