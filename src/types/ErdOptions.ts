import {ExternalMessageReceiver} from "./ExternalMessageReceiver";
import {MessagingSystem} from "./MessagingSystem";
import {ILogger} from "../util/ILogger";
import {LogLevel} from "./LogLevel";
import {GraphOptions} from "./GraphOptions";

export type ErdOptions = {
    elementId: string,
    initialModel?: {}|string,
    messagingSystem : MessagingSystem;
    onReceive ? : ExternalMessageReceiver
    logger ? : ILogger,
    logLevel ? : string;
    autoZoom : boolean;
    initialZoom : number;
    graphOptions : GraphOptions;
}
