export type GraphOptions = {
    rankdir: string,
    ranker: string,
    compound: boolean,
    nodesep: number,
    edgesep: number,
    ranksep: number,
    marginx: number,
    marginy: number
}
