export class LogLevel {
    static emerg = "emerg";
    static alert = "alert";
    static crit = "crit";
    static error = "error";
    static warning = "warnign";
    static notice = "notice";
    static info = "info";
    static debug = "debug";
    static none = "none";
}
