import {Message, MessageContentGenerics, MessageReceiver} from "../../system/Messenger";
import {MessageTransport} from "../MessageTransport";

export class PostMessage implements MessageTransport {

    /**
     * @param aListeners an array of callback functions to be called up on arrival of a specific message type
     */
    connect(aListeners: MessageReceiver[]):void {
        // addEventListener support for IE8
        function bindEvent(element, eventName, eventHandler) {
            if (element.addEventListener) {
                element.addEventListener(eventName, eventHandler, false);
            } else if (element.attachEvent) {
                element.attachEvent('on' + eventName, eventHandler);
            }
        }

        // Listen to messages from parent window
        bindEvent(window, 'message', (rawMessage: MessageEvent) => {
            if (typeof rawMessage.data !== "undefined") {
                try {
                    if (typeof rawMessage.data == 'string') {

                        let message: Message<MessageContentGenerics> = JSON.parse(rawMessage.data);
                        if (typeof (aListeners[message.topic]) != 'undefined') {
                            // Deliver message to the callback
                            aListeners[message.topic](message);
                        }
                    }
                } catch (e) {
                    // Webpack also sends these kind of messages, these can be ignored here.
                }
            }
        });
    }

    /**
     * @param topic - represents an address or subject
     * @param data - json object containing the actual data
     */
    send(topic: string, data: any):void {
        let oMessage = {
            topic: topic,
            data: data
        };
        // Make sure you are sending a string, and to stringify JSON
        window.parent.postMessage(JSON.stringify(oMessage), '*');
    }
}
