import {Message, MessageContentGenerics, MessageReceiver} from "../../system/Messenger";
import {MessageTransport} from "../MessageTransport";
import {ExternalMessageReceiver} from "../../types/ExternalMessageReceiver";


export class Callback implements MessageTransport {
    private internalHandlers: MessageReceiver[] = [];
    private externalMessageListeners : ExternalMessageReceiver[] = [];

    /**
     * @param listeners, an array of callback functions to be called up on arrival of a specific message type
     */
    connect(listeners: MessageReceiver[]): void {
        this.internalHandlers = listeners;
    }

    /**
     * This method is called when a message has arrived from outside of this system. It distributes the message to any
     * internal logic that is interested in the topic.
     * @param topic
     * @param message
     */
    onIncoming(topic: string, message: Message<MessageContentGenerics>) {
        this.internalHandlers.forEach((listener) => {

            if (topic != message.topic) {
                return;
            }
            if (typeof (listener[message.topic]) != 'undefined') {
                // Deliver message to the callback
                listener[message.topic](message);
            }
        })
    }

    /**
     * This method is used to register a callback located outside of the system. The callback should handle all the
     * actual modification of the datamodel
     * @param callback
     */
    onOutgoing(callback: ExternalMessageReceiver): void {
        this.externalMessageListeners.push(callback);
    }

    /**
     * This method is called by the internal process when a message with a destination outside this system needs to hear
     * about a certain event.
     *
     * @param topic the event that this message has information on.
     * @param message the actual message contents that the external process needs to act on.
     */
    send(topic: string, message: any): void {
        this.externalMessageListeners.forEach((callback) => {
            callback(topic, message);
        })
    }
}
