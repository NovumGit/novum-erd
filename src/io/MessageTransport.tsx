import {MessageReceiver} from "../system/Messenger";

export interface MessageTransport {
    connect(aListeners: MessageReceiver[]):void
    send(topic: string, data: any):void;
}
