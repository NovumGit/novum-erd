import {Promise} from "bluebird";
import {Test} from "../util/Test";
import {DataModel} from "../models/DataModel";
import {ErdOptions} from "../types/ErdOptions";
import {ILogger} from "../util/ILogger";

const fetch = require('node-fetch');
const queryString = require('query-string');

export class SchemaLoader {

    private static dataModel : DataModel = new DataModel();
    public static getDataModel():DataModel
    {
        return this.dataModel;
    }
    static load(element: Element, options : ErdOptions): Promise<string> {
        return new Promise<string>((resolve) => {

            const initialModel = options.initialModel;

            options.logger.debug("Property initalModel is of type: " + typeof(initialModel));

            if (typeof (initialModel) == "object") {
                options.logger.debug("Taking data model directly from initialModel property.");
                this.dataModel = initialModel;
                resolve();
            }

            if (typeof (initialModel) == "string") {
                options.logger.debug("Checking if initialModel contains a URL");
                this.fetchSchemaFromUrl(resolve, initialModel, options.logger);
            }

            if (!Test.emptyString(element.innerHTML)) {
                options.logger.debug("Checking of innerHTML property from element contains valid JSON data model");
                this.schemaFromElement(resolve, element.innerHTML, options.logger);
            }

            this.schemaFromGetVariable(resolve, options.logger);
        });
    }

    private static schemaFromGetVariable(resolve: any, logger : ILogger) {
        fetch.Promise = Promise;
        const getVariables = queryString.parse(location.search);

        if (getVariables.schema) {
            logger.debug("Found schema property in GET variables, assuming it contains a url, fetching that.")
            fetch(getVariables.schema)
            .then(res => res.json())
            .then(
                (json) => {
                    logger.debug("Parsing URL contents " + getVariables.schema);
                    const dataModel = JSON.parse(json);
                    logger.debug("Parsed successfully");
                    resolve(dataModel);
                }
            );
        }
    }

    private static schemaFromElement(resolve: any, innerHtml: string, logger : ILogger) {
        try {
            logger.debug("Trying to parse innerHTML to see if it has a valid datamodel");
            this.dataModel = JSON.parse(innerHtml);
            resolve('');
        } catch (e) {
            logger.debug("Parsing failed");
            return e;
        }
    }

    private static fetchSchemaFromUrl(resolve: any, schemaSource: string, logger : ILogger) {

        if (Test.url(schemaSource)) {
            logger.debug("initialModel property is a valid URL, fetching it now");

            fetch(schemaSource)
            .then(res => res.json())
            .then(
                (json) => {
                    logger.debug("Loaded data model from "  + schemaSource);

                    this.dataModel = JSON.parse(json);
                    resolve('');
                }
            );
        }
        else
        {
            logger.debug("No, initialModel property is not a url");
        }
    }
}
