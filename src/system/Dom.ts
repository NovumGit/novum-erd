export class Dom {

    static loaded(): Promise<string> {

        return new Promise<string>((resolve, reject) => {
            try {
                document.addEventListener('DOMContentLoaded', () => {
                    resolve();
                });
            } catch (e) {
                reject(e);
            }
        });
    }
}
