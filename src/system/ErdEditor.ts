import {ErdOptions} from "../types/ErdOptions";
import {ConsoleLogger} from "../util/ConsoleLogger";
import {MessagingSystem} from "../types/MessagingSystem";
import {Messenger} from "./Messenger";
import {PostMessage} from "../io/Transport/PostMessage";
import {Application} from "./Application";
import {Callback} from "../io/Transport/Callback";
import {LogLevel} from "../types/LogLevel";
import {ILogger} from "../util/ILogger";
import {ExternalMessageReceiver} from "../types/ExternalMessageReceiver";

const defaultOptions : ErdOptions = {
    autoZoom: false,
    initialZoom: 50,
    elementId: '#application',
    initialModel: {},
    messagingSystem: MessagingSystem.Callback,
    logLevel: LogLevel.none,
    graphOptions : {
        rankdir : "TB",
        ranker : "longest-path",
        compound : true,
        nodesep : 10,
        edgesep : 10,
        ranksep : 5,
        marginx : 4,
        marginy : 4
    }
};

export class ErdEditor {
    private directTransport: Callback = new Callback();
    private logger: ILogger;
    constructor(options: ErdOptions = defaultOptions) {
        if (options.logger) {
            this.logger = options.logger;
        }
        else
        {
            this.logger = new ConsoleLogger(options.logLevel);
        }
        if (options.messagingSystem == MessagingSystem.PostMessage) {
            this.logger.debug("Communication with ERD editor will use Window.postMessage() / Window.addEventListener()");
            Messenger.addTransport(new PostMessage());
        }
        else if (options.messagingSystem == MessagingSystem.Callback) {
            this.logger.debug("Communication with ERD editor will work with callbacks");
            Messenger.addTransport(this.directTransport);
        } else {
            this.logger.error("No valid messaging system is provided.");
            return;
        }

        Application.run(options).catch((e) => {
            this.logger.error("Application terminated");
            this.logger.error(e);
        });
    }
    onReceive(callback: ExternalMessageReceiver): void {
        this.logger.debug("Registering onReceive callback");
        this.directTransport.onOutgoing(callback);
    }
    send(topic, message): void {
        this.logger.debug("Received callback on topic " + topic);
        this.directTransport.onIncoming(topic, message);
    }
}
