import {DiagramModel, DiagramModelGenerics} from "@projectstorm/react-diagrams-core";
import {Field} from "../models/Field";
import {Table} from "../models/Table";
import {ErdNodeModel} from "../nodes/ErdNodeModel";
import {ErdLinkModel} from "../nodes/ErdLinkModel";
import {ErdLayerModel, ErdLayerModelGenerics} from "../nodes/ErdLayerModel";
import {ErdLinkLayerModel, ErdLinkLayerModelGenerics} from "../nodes/ErdLinkLayerModel";
import {ErdNodeLayerModel} from "../nodes/ErdNodeLayerModel";
import {LayerModel, LayerModelGenerics} from "@projectstorm/react-canvas-core";

enum LayerType {
    Links = 'diagram-links',
    Node = 'diagram-nodes',
}

export class SerializeHelper {

    serialized;
    constructor(model : DiagramModel<DiagramModelGenerics>)
    {
        this.serialized = model.serialize();
    }

    private findModel(field : Field):ErdNodeModel {
        const oModels = this.getNodes();
        for (let sModelsKey in oModels) {
            let model : ErdNodeModel = oModels[sModelsKey];
            if(model.table.name == field.table_name)
            {
                return model;
            }
        }
        throw new RangeError("The passed field was found in no models, has it been deleted?");
    }
    public removeField(field : Field):boolean
    {
        let model = this.findModel(field);
        model.removePortByName(field.name + '_in');
        model.removePortByName(field.name + '_out');

        model.table.removeField(field);

        /*
        const oModels = this.getModels();
        let newFields:Field[] = [];

        // 1. Loop over alle tabellen.
        for (let sModelsKey in oModels) {


            let table =  oModels[sModelsKey].table;
            let fields = table.fields;

            for(let i = 0; i < fields.length; i++)
            {
                if(table.name != field.table_name)
                {
                    continue;
                }

                if(fields[i].name == field.name && table.name == field.table_name)
                {
                    for (let portsKey in oModels[sModelsKey].getPorts())
                    {
                        if(oModels[sModelsKey].getPorts().hasOwnProperty(portsKey))
                        {
                            let portModel : PortModel = oModels[sModelsKey].getPorts()[portsKey];

                            for (let linksKey in portModel.links) {
                                portModel.removeLink(portModel.links[linksKey]);
                            }
                        }
                    }
                    continue;
                }
                newFields[i] = fields[i];
            }
            if(newFields.length === 0)
            {
                // Delete the whole node, no more fields left.
                delete oModels[sModelsKey];
                continue;
            }

            oModels[sModelsKey].table.fields = newFields;
        }
        this.setModels(oModels);
        */
        return false;
    }
    public getSerialized()
    {
        return this.serialized;
    }
    public setLinks(links:{[id:string] : ErdLinkModel})
    {
        this.serialized.layers[this.getLinkLayerIndex()] = links;
    }
    public getLinks():{[id:string] : ErdLinkModel}
    {
        return this.getLinkLayer().getModels();
    }
    private setNodes(nodes : {[id:string] : ErdNodeModel})
    {
        this.serialized.layers[this.getNodeLayerIndex()] = nodes;
    }
    private getNodes():{[id:string] : ErdNodeModel}
    {
        return this.getNodeLayer().getModels();
    }
    private getLayers<t extends LayerModel>():t[] {
        return this.serialized.layers;
    }
    private getLinkLayer():ErdLinkLayerModel<ErdLinkLayerModelGenerics> {
        return this.getLayers<ErdLinkLayerModel<ErdLinkLayerModelGenerics>>()[this.getLinkLayerIndex()]
    }
    private getNodeLayer():ErdNodeLayerModel {
        return this.getLayers<ErdNodeLayerModel>()[this.getNodeLayerIndex()];
    }

    private getLinkLayerIndex():number {
        const aLayers = this.getLayers();
        for (let i:number = 0; i < aLayers.length; i++)
        {
            if(aLayers[i].getOptions().type == LayerType.Links)
            {
                return i;
            }
        }
    }
    private getNodeLayerIndex():number {
        const aLayers = this.getLayers();
        for (let i:number = 0; i < aLayers.length; i++)
        {
            if(aLayers[i].getOptions().type == LayerType.Node)
            {
                return i;
            }
        }
    }
}
