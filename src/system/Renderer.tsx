import * as React from 'react';
import * as ReactDOM from 'react-dom';
import createEngine, {DiagramModel, DiagramEngine, DefaultLinkModel} from '@projectstorm/react-diagrams';
import {ErdNodeModel} from '../nodes/ErdNodeModel';
import {WorkspaceContainer} from "../nodes/WorkspaceContainer";
import {Table} from "../models/Table";
import {eventHandler} from "../nodes/EventHandler";
import {Field} from "../models/Field";
import {ErdNodeFactory} from "../nodes/ErdNodeFactory";
import {Relation} from "../models/Relation";
import {ErdLinkFactory} from "../nodes/ErdLinkFactory";
import {DistributeHelper} from "./DistributeHelper";
import {ErdDiagramModel} from "../nodes/ErdDiagramModel";
import {ErdOptions} from "../types/ErdOptions";
import {ILogger} from "../util/ILogger";

export class Renderer {

    private readonly engine: DiagramEngine;
    private canvas: Element;
    private options : ErdOptions

    private nodeIndex: number = 0;

    getEngine():DiagramEngine
    {
        return this.engine;
    }
    private getLogger():ILogger
    {
        return this.options.logger;
    }

    /**
     * @param options : ErdOptions
     */
    constructor(options : ErdOptions, canvasElement: Element) {
        // Set up our canvas to draw on without any content yet.
        this.options = options;
        this.engine = createEngine();
        this.canvas = canvasElement;
        this.engine.getNodeFactories().registerFactory(new ErdNodeFactory());
        this.engine.getLinkFactories().registerFactory(new ErdLinkFactory());
        this.engine.setModel(new DiagramModel());
    }

    drawDiagramOnScreen()
    {
        this.getLogger().debug('Renderer.rerender()');
        ReactDOM.render(
            <WorkspaceContainer renderer={this} logger={this.getLogger()} />, this.canvas);
    }

    render(jsonSchema) {
        this.getLogger().debug('Renderer.render() ' + JSON.stringify(jsonSchema));
        this.clear();

        if (typeof (jsonSchema) == "undefined" || typeof (jsonSchema['tables']) == "undefined") {
            return;
        }
        jsonSchema.tables.forEach((table: Table, index) => {
            this.addNode(table);
            this.nodeIndex = index;
        });

        jsonSchema.relations.forEach((relation: Relation) => {
            this.addLink(relation.from_model, relation.from_field, relation.to_model, relation.to_field);
        });
        /**
         * Calculate positions of all new models and relations before we draw them on screen.
         */

        // DistributeHelper.setOptions(this.options.graphOptions);
        // DistributeHelper.distribute(this.getEngine(), this.getLogger());

        /**
         * Output calculated results
         */
        // this.drawDiagramOnScreen();
    }

    public removeField(field: Field): void {
        /*
            1. Verwijder de property zelf.
            2. Verwijder de "in" en "out" ports.
            3. Verwijder spook links
         */

        let node = (this.engine.getModel() as ErdDiagramModel).findNode(field);

        node.removePortByName(field.table_name + '_' + field.name + '_in');
        node.removePortByName(field.table_name + '_' + field.name + '_out');

        node.table.removeField(field);
    }

    addProperty(field: Field): void {
        this.engine.getModel().getNodes().forEach((NodeModel: ErdNodeModel) => {
            if (NodeModel.table.name == field.table_name) {
                NodeModel.addProperty(field);
            }
        });
    }

    addNode(table: Table): void {
        this.nodeIndex++;
        const nodeX = new ErdNodeModel({color: 'rgb(0,192,255)', table: table});
        nodeX.setPosition(this.nodeIndex * 70, 50);
        this.engine.getModel().addNode(nodeX);

        this.engine.getModel().registerListener({
            eventDidFire: eventHandler('edit_model')
        });

    }

    /**
     * Een link is een verbinding tussen 2 poorten.
     *
     * @param tableFrom
     * @param fieldFrom
     * @param tableTo
     * @param fieldTo
     */
    addLink(tableFrom: string, fieldFrom: string, tableTo: string, fieldTo: string): void {
        const portOut = ErdNodeModel.getPortFromRegister(ErdNodeModel.makePortName(tableFrom, fieldFrom, 'out'));
        const portTo = ErdNodeModel.getPortFromRegister(ErdNodeModel.makePortName(tableTo, fieldTo, 'in'));
        const link = new DefaultLinkModel();
        link.setSourcePort(portOut);
        link.setTargetPort(portTo);
        this.engine.getModel().addLink(link);
    }

    /**
     * Removes all nodes and models and redraws the canvas
     */
    clear() {
        this.getLogger().debug('Renderer.clear() "Removes all nodes and models and redraws the canvas"');

        this.nodeIndex = 0;
        this.engine.getModel()
        this.engine.getModel().getLinks().forEach((LinkModel) => {
            this.engine.getModel().removeLink(LinkModel)
        });
        this.engine.getModel().getNodes().forEach((NodeModel) => {
            this.engine.getModel().removeNode(NodeModel)
        });
        // this.engine.setModel(this.model);

    }
}

