import {DiagramEngine} from '@projectstorm/react-diagrams';
import {RedistributeEngine} from "./RedistributeEngine";
import {PathFindingLinkFactory} from "@projectstorm/react-diagrams-routing";
import {ILogger} from "../util/ILogger";
import {GraphOptions} from "../types/GraphOptions";


export class DistributeHelper {

    private static graphOptions : GraphOptions = {
        rankdir: 'TB',
        ranker: 'longest-path',
        compound: true,
        nodesep: 10,
        edgesep: 10,
        ranksep: 5,
        marginx: 4,
        marginy: 4
    };

    static setOptions(graphOptions : GraphOptions)
    {
        this.graphOptions = graphOptions;
    }
    static distribute(engine : DiagramEngine, logger : ILogger)
    {
        logger.debug('DistributeHelper.distribute()');
        const distributeEngine = new RedistributeEngine({
            graph: this.graphOptions,
            includeLinks: true
        },  logger);


        logger.debug('DistributeHelper.distribute() "calling RedistributeEngine.redistribute()"');
        distributeEngine.redistribute(engine.getModel());

        engine
            .getLinkFactories()
            .getFactory<PathFindingLinkFactory>(PathFindingLinkFactory.NAME)
            .calculateRoutingMatrix();

    }
}
