import {Renderer} from "./Renderer";
import {SchemaLoader} from "./SchemaLoader";
import {Messenger} from "./Messenger";
import {ErdOptions} from "../types/ErdOptions";
import {Dom} from "./Dom";
import {DistributeHelper} from "./DistributeHelper";

export class Application {

    static run(options: ErdOptions): Promise<string> {

        return new Promise<string>(async (resolve, reject) => {
            try {
                options.logger.debug("Waiting for DOM to be loaded");
                await Dom.loaded();

                const canvasElement : Element = document.querySelector(options.elementId);
                const oRenderer = new Renderer(options, canvasElement);


                options.logger.debug("Attempt to load schema / datamodel");
                await SchemaLoader.load(canvasElement, options);
                // Drawing an empty diagram to begin with.
                await oRenderer.drawDiagramOnScreen();
                // Adding nodes to the screen
                await oRenderer.render(SchemaLoader.getDataModel());

                setTimeout(function() {
                // Redistrbuting nodes
                    DistributeHelper.setOptions(options.graphOptions);
                    DistributeHelper.distribute(oRenderer.getEngine(), options.logger);

                    // Draw nodes on their new (calculated positions)
                    oRenderer.getEngine().repaintCanvas();
                }, 1)







                /*
                console.warn("Options:");
                console.log(options);

                // oRenderer.getEngine().repaintCanvas();
                if(options.autoZoom)
                {
                    console.log("Zoom to fit");
                    oRenderer.getEngine().zoomToFit();
                }
                else if(options.initialZoom)
                {
                    oRenderer.getEngine().getModel().setZoomLevel(options.initialZoom);
                    oRenderer.getEngine().getModel().setOffset(0, 0);
                }

                console.log('aaaaaaaaaaa100aaaaaaaaaa');

                setTimeout(function() {
                    console.log('redistribute');
                    DistributeHelper.setOptions(options.graphOptions);
                    DistributeHelper.distribute(oRenderer.getEngine(), options.logger);
                    oRenderer.getEngine().repaintCanvas();
                }, 1000);
*/
                Messenger.start(oRenderer);


            } catch (e) {
                reject(e);
            }

        })
    }
}

