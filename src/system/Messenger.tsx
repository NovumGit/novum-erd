import {MessageTransport} from "../io/MessageTransport";
import {Table} from "../models/Table";
import {Field} from "../models/Field";
import {Renderer} from "./Renderer";

export interface Message<T> {
    topic: string;
    content: T
}

export interface MessageContentGenerics {
}

export type MessageReceiver = (payload: Message<MessageContentGenerics>) => void;

export class Messenger {

    private static isListening: boolean;
    private static aListeners: MessageReceiver[] = [];
    private static aTransports: MessageTransport[] = [];

    public static addTransport(messageTransport : MessageTransport)
    {
        this.aTransports.push(messageTransport);
    }


    public static start(renderer : Renderer) {
        Messenger.onReceive('model_added', (message: Message<Table>) => {
            renderer.addNode(Table.fromJson(message.content));
        });
        Messenger.onReceive('property_added', (message: Message<Field>) => {
            renderer.addProperty(Field.fromJson(message.content));
        });
        Messenger.onReceive('property_deleted', (message: Message<Field>) => {
            renderer.removeField(Field.fromJson(message.content));
        });

        this.aTransports.forEach((messageTransport) => {
            messageTransport.connect(this.aListeners);
        });
        this.isListening = true;
    }

    /**
     * Forwards messages originating from the ERD editor to external software
     * @param topic
     * @param data
     */
    public static send(topic: string, data: any) {
        this.aTransports.forEach((messageTransport) => {
            messageTransport.send(topic, data);
        });
    }

    /**
     * Handles incomming messages from external software and passes the message to the appropriate listener(s).
     * @param topic
     * @param callback
     */
    public static onReceive(topic :string, callback: MessageReceiver) {
        Messenger.aListeners[topic] = callback;
    }
}
