# NovumERD
NovumERD is a free and opensource software component that can be used to visualize, design and modify ERD diagrams. 

<p align="center">
<img width="300" src="https://raw.githubusercontent.com/antonboutkam/assets/master/novum/logo.svg" />
</p>

# Warning
This is my first opensource project on NPM and i lack experience on many levels. The code seems to run well but i may 
only think it does. (-: Please contact me if things go bad, thank you!

# Introduction

NovumERD is a free and opensource software component that can be used to visualize, design and modify ERD diagrams. 
This piece of software was designed to be integrated into other software as a component. This library or component 
generates a visualisation of a datamodel or anything else that can be captured inside an ERD diagram. It renders a 
visualisation that allows the user to intereact with the datamodel. Basically, create, modify, add or delete tables 
and/or columns. Once the user is done manipulating the datamodel it presses a commit or save button and the resulting 
datamodel is pushed back to the software that surrounds it. 

[NovumERD](https://gitlab.com/NovumGit/novum-erd#readme) is based on [React Diagrams](https://github.com/projectstorm/react-diagrams).

## In a Browser
In your html code add a html element with it's id set to be "application" (configurable) and include the library using 
a script tag. Then in another script tag start the engine by calling NovumErd.run(); This will initialize the editor with it's default
configuration. When you open the page in a webbrowser you should see a canvas / grid with some buttons but no data model.
  
```html
<div id="application"></div>
<script src="/yourinstalldir/novum-erd/dist/novum-erd.js"></script>
<script>
    <!-- Configuration info below -->
    const erdEditor = new Novum.ErdEditor({
        elementId: '#application',
        messagingSystem: Novum.MessagingSystem.Callback,
        logLevel: Novum.LogLevel.debug,
        logger : new Novum.ConsoleLogger()
    });
</script>
```

## TypeScript
```typescript
import {ErdEditor, MessagingSystem} from "erd-tool";
const initialDataModel = {};

ErdEditor.run({
    elementId: "#application",
    initialModel: initialDataModel,
    messagingSystem: MessagingSystem.Callback
});

ErdEditor.onReceive((topic : string, message) => {
    // Topic can be add_model,  edit_model, add_property edit_property
});
const topic = "model_added";
const message = {
    
}

ErdEditor.send(topic, message);
```

## Provide an initial data model
Next you will need to tell the engine what data model to render. The engine will look at various methods to obtain a 
data model. The first method is trough the dataModel property that you can pass inside an options object to the 
NovumErd.run() method. If the passed data has a type of "object" then the engine assumes the variable directly contains 
the data model. If the dataModel property has a type of "string", then it will try to use that as a url and will attempt 
to fetch the data model from there. When this turns out to be unsuccessful the datamodel will look at the innerHTML 
property of the element that it is supposed to draw the editor on. If JSON.parse will not throw any exceptions will 
use the contents of the element as it's initial datasource. The last method is trough a GET variable called schema, 
the GET variable should contain a reference to a URL that returns the initial data model. 

## Data model format
Please refer to the examples directory to see what the system is expecting as input. Please note that there is a section
tables and a section relations which defines the foreign keys

## Messages
The engine accepts and emits messages upon certain events. Each message has a topic and a payload, the topic tells the
recipient what the message is about, so it can be delegated to the appropriate handler. The payload contains the actual
message that needs to be handled.

### Message transport
There are currently two ways to communicate with the system. 

#### Direct communication
Up on initialisation you can pass a callback to handle incoming messages and call `NovumErd.send(myTopic, myMessage);`
whenever you would like to tell the engine about some state change.

```javascript
// Handle incomming message
NovumErd.onReceive((topic, message) => {
    // Your handling logic here.
});   

// Send messages to the engine
const myTopic = 'bla';
const myMessage = {};
NovumErd.send(myTopic, myMessage);
```
#### Window.postMessage() / Window.addEventListener()
As an alternative to providing callbacks you can use the Window.postMessage() and Window.addEventListener() 
functionality to communicate with the engine. In each direction a stringified JSON object is expected containing a
topic and a message property. This method is when the editor is loaded trough an iframe and you want to communicate
with the system from the parent window.  

# Configuration
A few configuration options can be given to the `NovumErd.run` method.
 
{
    elementId: string,
    initialModel?: {}|string,
    messagingSystem : MessagingSystem;
    onReceive? : ExternalMessageReceiver
    logLevel? : LogLevel.none
    logger? : ILogger
}

You can configure 
 Property       | Type                  | Default                   | Description                         
----------------| --------------------- | ------------------------- | ------------------------------------
elementId       | string                | #application              | Tells the editor what the id is of<br> the html element that should be used<br> to render the diagram in.
initialModel    | string|json|undefined | undefined                 | See [Provide an initial data model](#provide-an-initial-data-model)
messagingSystem | MessagingSystem       | MessagingSystem.Callback  | Specifies the way messages are passed<br> to the external software system. Can<br> be set to Callback or Postmessage. 
onReceive       | callback              | void                      | Only applicable when messagingSystem<br>is set to Callback, provide a callback<br>function with an argument topic (string)<br>and an argument message (json)
logLevel        | LogLevel              | LogLevel.none             | Available Log levels are taken over from [Winston](https://github.com/winstonjs/winston).<br> With the exception of LogLevel.none which turns off logging.
logger          | ILogger               | Logger                    | If you would like to log output you can provide a<br> logger that is compatible with the ILogger interface<br> which in turn should be compatbile with [Winston](https://github.com/winstonjs/winston) 


# Messages
Although some buttons added to the UI, you are expected to create your own panels / forms for manipulating the data 
model. Whenever this is done you can send a message to the ERD editor to notify the system about the changes. The ERD 
editor will update it's content based on whatever you are passing.

### Messages accepted by the ERD editor
 Topic           | Message               | Description                         
-----------------| --------------------- | -------------------------
model_added      | Table                 | You send this message after the user added a model in your own UI.<br> This then updates the visualisation by adding the model/table.              
property_added   | Field                 | You send this message after the user added a property in your own UI<br>.Please refer to the Field object or<br> see the example JSON for the definition<br> of a field                
property_deleted | Field                 | By sending `field.table_name` and `field.name`<br> in this message the property/field will be removed.  


### Messages send by the ERD editor
 Topic           | Message                 | Description                         
-----------------| ----------------------- | -------------------------
edit_model       | table_name              | The user wants to change some property of the<br> table. You show all the properties in a panel and allow<br>him/her to make modifications.              
edit_property    | table_name, column_name | The user wants to modify the column, you will<br>receive a table_name / column_name. <br>You show him a form to edit the property.                    
add_model        | {}                      | The user wants to create a table / model.<br>You should show him a form/modal    
add_property     | table_name              | When the user requests to add a property<br>to an existing table, you will receive<br>this message. The contents will be the<br> table name. So you should open some table<br> editing form or screen.
deploy           | DataModel               | When the user clicks on the deploy button<br>this message is emitted with the full <br> data model as it's contents. You should<br>do whatever needs to get done to deploy.





